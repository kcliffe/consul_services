from flask import Flask, request
import requests
from consul_utils import ConsulTool
import optparse

app = Flask(__name__)


@app.route('/')
def index():
    """
    Will be hit by the consul agent during health checks
    """
    return 'ok'


@app.route('/subscribe', methods=['POST'])
def subscribe():
    new_sub = request.get_json()

    existing_sub = next((subscriber for subscriber in subscribers if subscriber['name'] == new_sub['name']), None)
    if existing_sub is not None:
        return 'some sorta status code for duplicate ...'

    subscribers.append(new_sub)
    print "New subscriber registered a {address}".format(address=new_sub['address'])
    return 'ok'


@app.route('/pub', methods=['POST'])
def pub():
    msg = request.get_json()
    publish(msg)
    return 'ok'


def publish(msg):
    for s in subscribers:
        requests.post(s['address'], json={"msg": 'ok? {name}... {msg}'.format(name = s['name'], msg=msg)})


if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option
    parser.add_option("-p", "--port", help="Port " + "[default %s]" % 5001, default=5001)
    options, _ = parser.parse_args()
    port = int(options.port)

    print "Publisher starting on port {port}".format(port=port)

    subscribers = []
    ConsulTool.register_service('publisher', port, ['publisher of messages'])

    app.debug = True
    app.run(port=port)