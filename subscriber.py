from flask import Flask, request
import optparse
import requests
from consul_utils import ConsulTool


app = Flask(__name__)


@app.route('/')
def index():
    return 'ok'


@app.route('/onmsg', methods=['POST'])
def onmsg():
    msg = request.get_json()
    print 'received: {msg}'.format(msg=msg)
    return 'ok'


if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option
    parser.add_option('-n', '--name', help='my name...', default='fred')
    parser.add_option("-p", "--port", help="Port "+ "[default %s]" % 5002, default=5002)
    options, _ = parser.parse_args()
    port = int(options.port)

    print "Subscriber {name} starting on port {port}... \n".format(name=options.name, port=port)

    ConsulTool.register_service(options.name, port, [])

    # Get the publisher info
    publisher = ConsulTool.get_service('publisher')
    publisher_host = publisher['Address']
    publisher_port = publisher['ServicePort']
    publisher_uri = 'http://{host}:{port}/subscribe'.format(host=publisher_host, port=publisher_port)

    print "Publisher address is {addr}({host}), port {port} (and ... {tag0})".format(
        addr=publisher_host,
        host=publisher['Node'],
        port=publisher_port,
        tag0=publisher['ServiceTags'][0]
    )

    # "Subscribe" to messages
    requests.post(publisher_uri, json={ "name": options.name, "address": 'http://localhost:{port}/onmsg'.format(port=port)})

    app.debug = True
    app.run(port=int(port))