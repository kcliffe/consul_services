import consul

class ConsulTool():
    c = consul.Consul()

    @classmethod
    def register_service(cls, service_name, port, tags=[], addr='http://localhost'):
        try:
            url = '{addr}:{port}'.format(addr=addr,port=port)

            print "Registering service {name}".format(name=service_name)
            # Register Service
            cls.c.agent.service.register(service_name, port=port, tags=tags,
                                            check=consul.Check.http(url=url, interval=5))
            print "Service {name} registered".format(name=service_name)
        except:
            print "Failed to connect to the local consul instance. If it's not running, try:\r\n \
            consul agent -dev -data-dir={data.dir} -bind 127.0.0.1.\r\n\
        You can test with something like curl localhost:8500/v1/catalog/nodes"
            raise

    @classmethod
    def get_service(cls, service_name):
        print "Querying for service {name}".format(name=service_name)

        publisher = cls.c.catalog.service(service_name)
        if not publisher[1]:  # yuck
            print "Unable to locate service {name}. Bye.".format(name=service_name)
            exit()

        print "Service {name} found ok!".format(name=service_name)
        return publisher[1][0]